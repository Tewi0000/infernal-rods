package tewi0000.infernalrods.shared.material;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;

public class InfernalRodsMaterial implements IArmorMaterial {
    @Override public int getDurability(EquipmentSlotType slotIn) { return 0; }
    @Override public int getDamageReductionAmount(EquipmentSlotType slotIn) { return 1; }
    @Override public int getEnchantability() { return 0; }
    @Override public SoundEvent getSoundEvent() { return SoundEvents.ENTITY_BLAZE_HURT; }
    @Override public Ingredient getRepairMaterial() { return null; }
    @Override public String getName() { return "infernal_rods"; }
    @Override public float getToughness() { return 0; }
    @Override public float func_230304_f_() { return 0.2f; } // Knockback resistance
}
