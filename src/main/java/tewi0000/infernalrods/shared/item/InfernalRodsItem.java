package tewi0000.infernalrods.shared.item;

import net.minecraft.block.DispenserBlock;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;
import tewi0000.infernalrods.InfernalRodsMod;
import tewi0000.infernalrods.shared.material.InfernalRodsMaterial;

public class InfernalRodsItem extends ArmorItem {
    public InfernalRodsItem(Item.Properties properties) {
        super(new InfernalRodsMaterial(), EquipmentSlotType.CHEST, properties);
        DispenserBlock.registerDispenseBehavior(this, ArmorItem.DISPENSER_BEHAVIOR);
    }

    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        if (worldIn.isRemote) {
            if (random.nextInt(32) == 0) {
                worldIn.playSound(entityIn.getPosX() + 0.5D,
                        entityIn.getPosY() + 0.5D,
                        entityIn.getPosZ() + 0.5D,
                        SoundEvents.ENTITY_BLAZE_BURN,
                        entityIn.getSoundCategory(), 1.0F + random.nextFloat(),
                        random.nextFloat() * 0.7F + 0.3F,
                        false);
            }

            if (random.nextInt(8) == 0) {
                if (!worldIn.isRemote || Minecraft.getInstance().gameRenderer.getActiveRenderInfo().isThirdPerson()) {
                    worldIn.addParticle(ParticleTypes.LARGE_SMOKE,
                            entityIn.getPosXRandom(0.5D),
                            entityIn.getPosYRandom(),
                            entityIn.getPosZRandom(0.5D),
                            0.0D, 0.0D, 0.0D);
                }
            }

        }
    }

    @Override
    public String getArmorTexture(ItemStack itemstack, Entity entity, EquipmentSlotType slot, String layer) {
        return InfernalRodsMod.MOD_ID + ":textures/models/armor/empty.png";
    }
}