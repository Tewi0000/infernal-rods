package tewi0000.infernalrods;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.renderer.entity.ArmorStandRenderer;
import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

import tewi0000.infernalrods.client.layer.InfernalRodsLayer;
import tewi0000.infernalrods.shared.item.InfernalRodsItem;

@Mod.EventBusSubscriber
@Mod(InfernalRodsMod.MOD_ID)
public class InfernalRodsMod {
    public static final String MOD_ID = "infernalrods";

    public InfernalRodsMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().register(this);
        MinecraftForge.EVENT_BUS.register(new ModEvents());
    }

    private void setup(final FMLClientSetupEvent event) {
        Minecraft.getInstance()
                 .getRenderManager()
                 .getSkinMap()
                 .forEach((string, render) -> render.addLayer(new InfernalRodsLayer<>(render, 7F, 8.5F, (9F))));

        EntityRendererManager rm = Minecraft.getInstance().getRenderManager();
        rm.renderers.forEach((e, r) -> {
            if (r instanceof BipedRenderer) {
                ((BipedRenderer)r).addLayer(new InfernalRodsLayer<>((BipedRenderer<?, ?>)r, 7F, 8.5F, (9F)));
            }
        });

        ArmorStandRenderer armorStandRenderer = (ArmorStandRenderer)rm.renderers.get(EntityType.ARMOR_STAND);
        armorStandRenderer.addLayer(new InfernalRodsLayer<>(armorStandRenderer, 5F, 7F, 9F));
    }

    @SubscribeEvent
    void registerItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(ModItems.INFERNAL_RODS);
    }

    @SubscribeEvent
    static void onPlayerTick(TickEvent.PlayerTickEvent event) {
        if (event.player.inventory.armorItemInSlot(2).getItem() instanceof InfernalRodsItem) {
            if (event.player.fallDistance >= 3) {
                if (!event.player.isCrouching()) {
                    event.player.addPotionEffect(new EffectInstance(Effects.SLOW_FALLING, 80, 1, false, false));
                }
            }

            if (event.player.isCrouching()) {
                event.player.removeActivePotionEffect(Effects.SLOW_FALLING);
            }

            if (event.player.func_233570_aj_()) { // onGround()
                event.player.removeActivePotionEffect(Effects.SLOW_FALLING);
                event.player.fallDistance = 0;
            }

        }
    }

    static class ModEvents {
        private int jumps;

        @SubscribeEvent
        public void onInput(InputEvent.KeyInputEvent event) {
            if (event.getKey() == 32 && event.getAction() == 1) {
                Minecraft mc = Minecraft.getInstance();
                if (mc.currentScreen == null) {
                    ClientPlayerEntity player = mc.player;

                    if (player != null && player.func_233570_aj_()) { // onGround()
                        jumps = 0;
                    }

                    int maxJumps = 12;
                    if (jumps < maxJumps) {
                        for (ItemStack itemStack : player.getArmorInventoryList()) {
                            if (itemStack.getItem() instanceof InfernalRodsItem) {
                                player.setMotion(player.getMotion().mul(1,0,1).add(0,0.75,0));
                                jumps++;
                                for (int i = 0; i < 3; i++)
                                    player.world.addParticle(ParticleTypes.LARGE_SMOKE,
                                            player.getPosXRandom(0.5D),
                                            player.getPosYRandom(),
                                            player.getPosZRandom(0.5D),
                                            0.0D, -0.1D, 0.0D);
                                break;
                            }
                        }
                    }
                }

            }

        }

    }

}
