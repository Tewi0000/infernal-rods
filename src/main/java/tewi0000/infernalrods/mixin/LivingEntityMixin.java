package tewi0000.infernalrods.mixin;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import tewi0000.infernalrods.shared.item.InfernalRodsItem;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin {
    @Shadow
    NonNullList<ItemStack> armorArray;

    @Overwrite
    public boolean func_230270_dK_() {
        return armorArray.get(2).getItem() instanceof InfernalRodsItem;
    }
    public boolean func_230279_az_() {
        return armorArray.get(2).getItem() instanceof InfernalRodsItem;
    }
}
